export interface IUserSignUp {
  name: string;
  email: string;
  password: string;
}

export class UserSignUp {

  name: string;
  email: string;
  private _password: string;

  constructor(userData: IUserSignUp) {
    this.name = userData.name;
    this.email = userData.email;
    this._password = userData.password;
  }

}
