export interface IUser {
  id: number;
  name: string;
  email: string;
  avatar?: { url: string };
  description?: string;
}

export class User implements IUser {
  id: number;
  name: string;
  email: string;
  avatar?: { url: string };
  description?: string;
}
