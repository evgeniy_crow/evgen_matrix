import { Injectable } from '@angular/core';

import { UserSignUp } from "../../models/user-sign-up";

import { Observable } from "rxjs/Observable";
import 'rxjs/add/observable/of';

@Injectable()
export class UsersMockProvider {
  usersList: UserSignUp[] = [];

  defaultUser: any = {
    "name": "Guest"
  };


  constructor() {
    let usersData = [
      {
        "name": "Burt Bear",
        "email": "test@example.com",
        "password": "test"
      },
      {
        "name": "Test Human",
        "email": "test@example.com",
        "password": "test"
      },
      {
        "name": "Donald Duck",
        "email": "test@example.com",
        "password": "test"
      }
    ];

    for (let userData of usersData) {
      this.usersList.push(new UserSignUp(userData));
    }
  }

  query(params?: any) {
    // if (!params) {
    //   return this.usersList;
    // }
    //
    // return this.usersList.filter((item) => {
    //   for (let key in params) {
    //     let field = item[key];
    //     if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
    //       return item;
    //     } else if (field == params[key]) {
    //       return item;
    //     }
    //   }
    //   return null;
    // });
    return Observable.of<any>(["1"]);
  }

  add(item: UserSignUp) {
    this.usersList.push(item);
  }

  delete(item: UserSignUp) {
    this.usersList.splice(this.usersList.indexOf(item), 1);
  }
}
