import { HttpClient, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';

//mocks
import { Observable } from "rxjs/Observable";
import { UsersMockProvider } from "../../mocks/providers/users-mock.provider";
import { UserSignUp } from "../../models/user-sign-up";

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {
  dataFromMock: Observable<UserSignUp[]>;
  url: string = 'localhost:8100/src/mocks/providers/users';

  constructor(public http: HttpClient, private usersMockProvider: UsersMockProvider) {
  }

  get(endpoint: string, params?: any, reqOpts?: any) {
    if (!reqOpts) {
       reqOpts = {
        params: new HttpParams()
      };
    }

    // Support easy query params for GET requests
    if (params) {
      reqOpts.params = new HttpParams();
      for (let k in params) {
        reqOpts.params.set(k, params[k]);
      }
    }

    return this.http.get(this.url + '/' + endpoint, reqOpts);
  }

  post(endpoint: string, body: any, reqOpts?: any) {
    this.dataFromMock = this.usersMockProvider.query(body);
    console.log('res on api', this.dataFromMock);
    return this.dataFromMock;
    // return this.http.post(this.url + '/' + endpoint, body, reqOpts);
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    return this.http.put(this.url + '/' + endpoint, body, reqOpts);
  }

  delete(endpoint: string, reqOpts?: any) {
    return this.http.delete(this.url + '/' + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    return this.http.put(this.url + '/' + endpoint, body, reqOpts);
  }
}
