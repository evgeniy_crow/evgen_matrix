import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';

import { Api } from '../api/api';

/**
 * Most apps have the concept of a User. This is a simple provider
 * with stubs for sign-in/sign-up/etc.
 *
 * This User provider makes calls to our API at the `sign-in` and `sign-up` endpoints.
 *
 * By default, it expects `sign-in` and `sign-up` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   user: {
 *     // User fields your app needs, like "id", "name", "email", etc.
 *   }
 * }Ø
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */
@Injectable()
export class AuthProvider {
  _user: any;

  constructor(public api: Api) { }

  /**
   * Send a POST request to our sign-in endpoint with the data
   * the user entered on the form.
   */
  signIn(accountData: any) {
    let seq = this.api.post('signin', accountData).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      console.log('res on auth', res);
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  /**
   * Send a POST request to our sign-up endpoint with the data
   * the user entered on the form.
   */
  signUp(accountInfo: any) {
    let seq = this.api.post('signup', accountInfo).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  /**
   * Log the user out, which forgets the session
   */
  logout() {
    this._user = null;
  }

  /**
   * Process a sign-in/sign-up response to store user data
   */
  _loggedIn(resp) {
    this._user = resp.user;
  }

  get currentUser() {
    return this._user
  }
}
