import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { AuthProvider } from '../../providers/providers';
import { MainPage } from '../pages';
import { IUserSignUp } from "../../models/user-sign-up";
import { UserHomePage } from "../user-home/user-home.page";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'sign-in.html'
})
export class SignInPage {
  // The account fields for the sign-in form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  userData: IUserSignUp = {
    name: 'Test Human',
    email: 'test@example.com',
    password: 'test'
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(public navCtrl: NavController,
    public authProvider: AuthProvider,
    public toastCtrl: ToastController,
    public translateService: TranslateService) {

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value; // Проверить на ошибки!
    })
  }

  // Attempt to sign-in in through our User service
  doSignIn() {
    this.authProvider.signIn(this.userData).subscribe((resp) => {
      this.navCtrl.push(UserHomePage);
    }, (err) => {
      this.navCtrl.push(MainPage);
      // Unable to log in
      let toast = this.toastCtrl.create({
        message: this.loginErrorString,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    });
  }
}
