import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { AuthProvider } from '../../providers/providers';
import { UserHomePage } from "../user-home/user-home.page";

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html'
})
export class SignUpPage {
  // The account fields for the sign-in form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { name: string, email: string, password: string } = {
    name: 'Test Human',
    email: 'test@example.com',
    password: 'test'
  };

  // Our translated text strings
  private signUpErrorString: string;

  constructor(public navCtrl: NavController,
    public user: AuthProvider,
    public toastCtrl: ToastController,
    public translateService: TranslateService) {

    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signUpErrorString = value;
    })
  }

  doSignUp() {
    // Attempt to sign-in in through our User service
    this.user.signUp(this.account).subscribe((resp) => {
      this.navCtrl.push(UserHomePage);
    }, (err) => {

      this.navCtrl.push(UserHomePage);

      // Unable to sign up
      let toast = this.toastCtrl.create({
        message: this.signUpErrorString,
        duration: 4000,
        position: 'top'
      });
      toast.present();
    });
  }
}
