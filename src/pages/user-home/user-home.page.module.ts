import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { UserHomePage } from "./user-home.page";

@NgModule({
  declarations: [
    UserHomePage,
  ],
  imports: [
    IonicPageModule.forChild(UserHomePage),
    TranslateModule.forChild()
  ],
  exports: [
    UserHomePage
  ]
})
export class UserHomePageModule { }
