import { Component, OnInit } from '@angular/core';
import { IonicPage, ModalController, NavController } from 'ionic-angular';


import { AuthProvider } from "../../providers/auth/auth.provider";
import { IUser } from "../../models/user";
import { UsersMockProvider } from "../../mocks/providers/users-mock.provider";

@IonicPage()
@Component({
  selector: 'user-home-page',
  templateUrl: 'user-home.page.html'
})
export class UserHomePage implements OnInit {

  public currentUser: IUser;
  // usersList: IUser[];

  constructor(public navCtrl: NavController,
              public authProvider: AuthProvider,
              public usersMock: UsersMockProvider,
              public modalCtrl: ModalController) {
  }

  public ngOnInit() {
    this.currentUser = this.authProvider.currentUser();
    // this.usersList = this.usersMock.query();
  }

}
